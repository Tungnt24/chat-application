'use strict';
var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');


var stompClient = null;
var username = null;
var user_id = 0;
var joined = false;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

var websocket = new WebSocket("ws://103.56.158.42:8080/WebSocket2/chatRoomServer");
websocket.onopen = function (message) { processOpen(message); };
websocket.onmessage = function (message) { onMessageReceived(message); };
websocket.onclose = function (message) { processClose(message); };
websocket.onerror = function (message) { onError(message); };

function processOpen(message) {
    if(websocket.readyState == WebSocket.OPEN){
        console.log("open" + user_id)
    }else{
        console.log("closed")
    }
    console.log("connected")
}

function processClose(message) {
    if(websocket.readyState == WebSocket.OPEN){
        console.log("d_open")
    }else{
        console.log("d_closed")
    }
    console.log("Disconnected...")
}


function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
    console.log("error")
}


function sendMessage() {
    var messageContent = messageInput.value.trim();
    console.log(messageContent)
    if (typeof websocket != 'undefined' && websocket.readyState == WebSocket.OPEN) {
        websocket.send(messageContent);
        messageInput.value = '';
    }
}


function onMessageReceived(payload) {
    console.log(payload)
    var username = "NaN";
    var message = "NaN";
    var data = payload.data.split(":");
    username = data[0];
    message = data[1].trim();
    
    console.log("recieved: " + message);
    var messageElement = document.createElement('li');

    messageElement.classList.add('chat-message');

    var avatarElement = document.createElement('i');
    var avatarText = document.createTextNode(username);
    avatarElement.appendChild(avatarText);
    avatarElement.style['background-color'] = getAvatarColor(username);

    messageElement.appendChild(avatarElement);

    var usernameElement = document.createElement('span');
    var usernameText = document.createTextNode(username);
    usernameElement.appendChild(usernameText);
    messageElement.appendChild(usernameElement);

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}

// enter de gui tin nhan
window.addEventListener('keydown', function (e) {
    if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
        if (e.target.nodeName == 'INPUT' && e.target.type == 'text') {
            e.preventDefault();
            document.getElementById('btn').click();
            return false;
        }
    }
},true);